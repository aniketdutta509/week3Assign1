package Assignment1;

import java.util.ArrayList;
import java.util.function.Function;

public class Employee_Q2 {
	int id;
	String name;
	double salary;
	
	Employee_Q2(int i, String n, double s){
		id = i;
		name = n;
		salary = s;
	}
	
	double incrSal() {
		return salary+= salary*0.1;
	}

	
	public static void main(String[] args) {
		
		ArrayList<Employee_Q2> em = new ArrayList<Employee_Q2>();
		
		em.add(new Employee_Q2(12,"Akhil",30000));
		em.add(new Employee_Q2(13,"Rimli",35000));
		
		em.stream().map(p->p.incrSal()).forEach(System.out::println);
		
	}

}
